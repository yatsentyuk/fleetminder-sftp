package com.fleetminder.sftp.server;

import com.fleetminder.sftp.service.AuthenticationService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import org.apache.sshd.common.file.virtualfs.VirtualFileSystemFactory;
import org.apache.sshd.server.SshServer;

import org.apache.sshd.server.keyprovider.SimpleGeneratorHostKeyProvider;
import org.apache.sshd.server.scp.ScpCommandFactory;
import org.apache.sshd.server.subsystem.sftp.SftpSubsystemFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Paths;
import java.util.*;

@Slf4j
@Service
public class SftpServer {

    private final String RSA_PRIVATE_KEY;

    private final int PORT = 2222;

    @Autowired
    SftpServer(@Value("sftp.private-key-path") String key) {
        RSA_PRIVATE_KEY = key;
    }

    @Value("${sftp.file.dir}")
    private String pathname;

    @Autowired
    private AuthenticationService authenticationService;

    @PostConstruct
    public void startServer() {
        start();
    }


    @SneakyThrows
    private void start() {
        SshServer sshd = SshServer.setUpDefaultServer();
        sshd.setPort(PORT);
        sshd.setKeyPairProvider(new SimpleGeneratorHostKeyProvider(Paths.get(RSA_PRIVATE_KEY)));
        sshd.setSubsystemFactories(Collections.singletonList(new SftpSubsystemFactory()));

        sshd.setPasswordAuthenticator((username, password, session) -> {
            File f = new File(String.format("%s%s/", pathname, username));
            f.mkdir();

            VirtualFileSystemFactory fsFactory = new VirtualFileSystemFactory();
            fsFactory.setDefaultHomeDir(f.toPath());
            sshd.setFileSystemFactory(fsFactory);

            return authenticationService.validPassword(username, password);
        });

        sshd.setCommandFactory(new ScpCommandFactory());

        sshd.start();
        log.info("SFTP server started");
    }
}
