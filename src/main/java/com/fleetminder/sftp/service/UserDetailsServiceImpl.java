package com.fleetminder.sftp.service;

import com.fleetminder.sftp.repository.UserRepository;
import lombok.var;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String username) {
        var user = userRepository.findByUsername(username);

        if (user.isPresent()){
            Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
            String password = user.get().getPassword();

            return new User(username, password, true, true, true, true, grantedAuthorities);
        } else {
            throw new UsernameNotFoundException(username);
        }
    }
}
