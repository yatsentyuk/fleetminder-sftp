package com.fleetminder.sftp.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.encoding.MessageDigestPasswordEncoder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AuthenticationService {

    @Qualifier("userDetailsServiceImpl")
    @Autowired
    private UserDetailsService userDetailsService;

    private String ALGORITHM = "SHA-256";

    MessageDigestPasswordEncoder encoder = new MessageDigestPasswordEncoder(ALGORITHM);

    public boolean validPassword(String username, String password) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);

        return encoder.encodePassword(password, null).equals(userDetails.getPassword());
    }
}
